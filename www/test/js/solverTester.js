
QUnit.test( "Solver - verify valid solution", function( assert ) {
    var validSolution = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var solver = new Solver();
    solver._initializeConstants(81);
    solver._initializeCellIndexLookupTables();
    assert.ok(solver._verifySolutionCompliance(validSolution), "_verifySolutionCompliance recognized valid solution");
});

QUnit.test( "Solver - verify invalid solution", function( assert ) {
    var solver = new Solver();
    solver._initializeConstants(81);
    solver._initializeCellIndexLookupTables();
    
    // invalid - value higher than 9
    var invalidSolution = [10,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution), "_verifySolutionCompliance number outside valid range - high");
    // invalid - value lower than 1
    invalidSolution = [-1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution), "_verifySolutionCompliance number outside valid range - low");
    // invalid - dup value in rows
    invalidSolution = [1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution), "_verifySolutionCompliance dup in row");
    // invalid - dup value in column
    invalidSolution = [1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution), "_verifySolutionCompliance dup in column");
    // invalid - dup value in cluster
    invalidSolution = [1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution), "_verifySolutionCompliance number dup in cluster");
    // invalid - series of pseudo-random alterations to the valid solution
    var invalidSolution01 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,7,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution02 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,2,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution03 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,8,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution04 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,2,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution05 = [1,2,3,4,5,6,1,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution06 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,1,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution07 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,2,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution08 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,9,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution09 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,5,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution10 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,3,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution11 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,1,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution12 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,3,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution13 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,3,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution14 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,7,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution15 = [1,7,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution16 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,7,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution17 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,7,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var invalidSolution18 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,7,4,1,5,7,2];
    var invalidSolution19 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,7];
    var invalidSolution20 = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,5,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    assert.ok(!solver._verifySolutionCompliance(invalidSolution01));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution02));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution03));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution04));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution05));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution06));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution07));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution08));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution09));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution10));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution11));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution12));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution13));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution14));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution15));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution16));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution17));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution18));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution19));
    assert.ok(!solver._verifySolutionCompliance(invalidSolution20));
});

QUnit.test( "Solver - verify valid solution with puzzle check", function( assert ) {
    var validSolution   = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var validPuzzle1    = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,2];
    var validPuzzle2    = [1,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,7,0];
    var validPuzzle3    = [0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0];
    var validPuzzle4    = [0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var validPuzzle5    = [0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var invalidPuzzle1  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var invalidPuzzle2  = [0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var invalidPuzzle3  = [0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var invalidPuzzle4  = [0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var invalidPuzzle5  = [1,2,3,4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,1,6,7,4,8,9,5,8,7,5,9,1,2,3,6,4,6,9,4,5,3,8,2,1,7,3,1,7,2,6,5,9,4,8,5,4,2,8,9,7,6,3,1,9,6,8,3,4,1,5,7,1];
    var solver = new Solver();
    solver._initializeConstants(81);
    solver._initializeCellIndexLookupTables();
    assert.ok(solver._verifySolutionCompliance(validSolution), "_verifySolutionCompliance recognized valid solution");
    assert.ok(solver._verifySolutionCompliance(validSolution, validPuzzle1), "_verifySolutionCompliance recognized valid solution against puzzle 1");
    assert.ok(solver._verifySolutionCompliance(validSolution, validPuzzle2), "_verifySolutionCompliance recognized valid solution against puzzle 2");
    assert.ok(solver._verifySolutionCompliance(validSolution, validPuzzle3), "_verifySolutionCompliance recognized valid solution against puzzle 3");
    assert.ok(solver._verifySolutionCompliance(validSolution, validPuzzle4), "_verifySolutionCompliance recognized valid solution against puzzle 4");
    assert.ok(solver._verifySolutionCompliance(validSolution, validPuzzle5), "_verifySolutionCompliance recognized valid solution against puzzle 5");

    assert.ok(!solver._verifySolutionCompliance(validSolution, invalidPuzzle1), "_verifySolutionCompliance recognized invalid solution against bad puzzle 1");
    assert.ok(!solver._verifySolutionCompliance(validSolution, invalidPuzzle2), "_verifySolutionCompliance recognized invalid solution against bad puzzle 2");
    assert.ok(!solver._verifySolutionCompliance(validSolution, invalidPuzzle3), "_verifySolutionCompliance recognized invalid solution against bad puzzle 3");
    assert.ok(!solver._verifySolutionCompliance(validSolution, invalidPuzzle4), "_verifySolutionCompliance recognized invalid solution against bad puzzle 4");
    assert.ok(!solver._verifySolutionCompliance(validSolution, invalidPuzzle5), "_verifySolutionCompliance recognized invalid solution against bad puzzle 5");
});

QUnit.test( "Solver - unsolvable puzzle", function( assert ) {
    var solver = new Solver();
    solver._initializeConstants(81);
    solver._initializeCellIndexLookupTables();
    var unsolvablePuzzle1    = [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var unsolvablePuzzle2    = [1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var unsolvablePuzzle3    = [1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    assert.ok(!solver.solve(unsolvablePuzzle1), "unsolvable puzzle1");
    assert.ok(!solver.solve(unsolvablePuzzle2), "unsolvable puzzle2");
    assert.ok(!solver.solve(unsolvablePuzzle3), "unsolvable puzzle3");
});


QUnit.test( "Solver - solve hard puzzle", function( assert ) {
    var done = assert.async();
    var hardPuzzle = "6.2.5.........3.4..........43...8....1....2........7..5..27...........81...6.....";
    sudoqu.PuzzleReader.getInstance().readFromString(hardPuzzle,  function(puzzleArray){
        var solver = new Solver();
        var solution = solver.solve(puzzleArray[0]);
        assert.ok(solution, "solve hard puzzle");
        done();
    });
});

/** 
 * Now that we have thoroughly tested our ability to read puzzles and validate solutions, launch
 * the 'top 95' test.  While running this test the browser may alert the user that the 
 * page has gone unresponsive.  Just click wait to allow it to continue.
 */
QUnit.test( "Solver - top95 test", function( assert ) {
    var done = assert.async();
    sudoqu.PuzzleReader.getInstance().readFromUrl("res/puzzles/top95.txt",  function(puzzleArray){
        var solver = new Solver();
        for(var i = 0; i < puzzleArray.length; i++){
            var solution = solver.solve(puzzleArray[i]);
            assert.ok(solution, "top95 test #" + i);
        }
        done();
    });
});

/** 
 * Push the limit and check if we can solve puzzles of other dimensions
 */
QUnit.test( "Solver - Solve 4x4 puzzle", function( assert ) {
    var solver = new Solver();
    var puzzleNumbers = [0,0,4,0,4,0,3,0,0,4,0,3,0,1,0,0]
    var solution = solver.solve(puzzleNumbers);
    assert.ok(solution, "Solved 4x4 puzzle");
});

QUnit.test( "Solver - Solve 16x16 puzzle", function( assert ) {
    var solver = new Solver();
    var puzzleNumbers = [ 1, 0, 0, 2, 3, 4, 0, 0,12, 0, 6, 0, 0, 0, 7, 0,
                          0, 0, 8, 0, 0, 0, 7, 0, 0, 3, 0, 0, 9,10, 6,11,
                          0,12, 0, 0,10, 0, 0, 1, 0,13, 0,11, 0, 0,14, 0,
                          3, 0, 0,15, 2, 0, 0,14, 0, 0, 0, 9, 0, 0,12, 0,
                         13, 0, 0, 0, 8, 0, 0,10, 0,12, 2, 0, 1,15, 0, 0,
                          0,11, 7, 6, 0, 0, 0,16, 0, 0, 0,15, 0, 0, 5,13,
                          0, 0, 0,10, 0, 5,15, 0, 0, 4, 0, 8, 0, 0,11 ,0,
                         16, 0, 0, 5, 9,12, 0, 0, 1, 0, 0, 0, 0, 0, 8, 0,
                          0, 2, 0, 0, 0, 0, 0,13, 0, 0,12, 5, 8, 0, 0, 3,
                          0,13, 0, 0,15, 0, 3, 0, 0,14, 8, 0,16, 0, 0, 0,
                          5, 8, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0,13, 9,15, 0,
                          0, 0,12, 4, 0, 6,16, 0,13, 0, 0, 7, 0, 0, 0, 5,
                          0, 3, 0, 0,12, 0, 0, 0, 6, 0, 0, 4,11, 0, 0,16,
                          0, 7, 0, 0,16, 0, 5, 0,14, 0, 0, 1, 0, 0, 2, 0,
                         11, 1,15, 9, 0, 0,13, 0, 0, 2, 0, 0, 0,14, 0, 0,
                          0,14, 0, 0, 0,11, 0, 2, 0, 0,13, 3, 5, 0, 0,12];
    // drum roll..........                          
    var solution = solver.solve(puzzleNumbers);
    assert.ok(solution, "Solved 16x16 puzzle");
});

function bigTest(){
    QUnit.test( "Solver2222 - Detect invalid 16x16 puzzle", function( assert ) {
        var solver = new Solver();
        var puzzleNumbers = [ 1, 0, 0, 2, 3, 4, 0, 0,12, 0, 6, 0, 0, 0, 7, 0,
                              0, 0, 8, 0, 0, 0, 7, 0, 0, 3, 0, 0, 9,10, 6,11,
                              0,12, 0, 0,10, 0, 0, 1, 0,13, 0,11, 0, 0,14, 0,
                              3, 0, 0,15, 2, 0, 0,14, 0, 0, 0, 9, 0, 0,12, 0,
                             13, 0, 0, 0, 8, 0, 0,10, 0,12, 2, 0, 1,15, 0, 0,
                              0,11, 7, 6, 0, 0, 0,16, 0, 0, 0,15, 0, 0, 5,13,
                              0, 0, 0,10, 0, 5,15, 0, 0, 4, 0, 8, 0, 0,11 ,0,
                             16, 0, 0, 5, 9,12, 0, 0, 1, 0, 0, 0, 0, 0, 8, 0,
                              0, 2, 0, 0, 0, 0, 0,13, 0, 0,12, 5, 8, 0, 0, 3,
                              0,13, 0, 0,15, 0, 3, 0, 0,14, 8, 0,16, 0, 0, 0,
                              5, 8, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0,13, 9,15, 0,
                              0, 0,12, 4, 0, 6,16, 0,13, 0, 0, 7, 0, 0, 0, 5,
                              0, 3, 0, 0,12, 0, 0, 0, 6, 0, 0, 4,11, 0, 0,16,
                              0, 7, 1, 0,16, 0, 5, 0,14, 0, 0, 1, 0, 0, 2, 0,
                             11, 1,15, 9, 0, 0,13, 0, 0, 2, 0, 0, 0,14, 0, 0,
                              0,14, 0, 0, 0,11, 0, 2, 0, 0,13, 3, 5, 0, 0,12];
        // drum roll..........                          
        var solution = solver.solve(puzzleNumbers);
        assert.ok(!solution, "Invalid 16x16 puzzle detected");
    });
}

QUnit.test( "Solver - Detect invalid 16x16 puzzle", function( assert ) {
    var solver = new Solver();
    var puzzleNumbers = [ 1, 0, 0, 2, 3, 4, 0, 0,12, 0, 6, 0, 0, 0, 7, 0,
                          0, 0, 8, 0, 0, 0, 7, 0, 0, 3, 0, 0, 9,10, 6,11,
                          0,12, 0, 0,10, 0, 0, 1, 0,13, 0,11, 0, 0,14, 0,
                          3, 0, 0,15, 2, 0, 0,14, 0, 0, 0, 9, 0, 0,12, 0,
                         13, 0, 0, 0, 8, 0, 0,10, 0,12, 2, 0, 1,15, 0, 0,
                          0,11, 7, 6, 0, 0, 0,16, 0, 0, 0,15, 0, 0, 5,13,
                          0, 0, 0,10, 0, 5,15, 0, 0, 4, 0, 8, 0, 0,11 ,0,
                         16, 0, 0, 5, 9,12, 0, 0, 1, 0, 0, 0, 0, 0, 8, 0,
                          0, 2, 0, 0, 0, 0, 0,13, 0, 0,12, 5, 8, 0, 0, 3,
                          0,13, 0, 0,15, 0, 3, 0, 0,14, 8, 0,16, 0, 0, 0,
                          5, 8, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0,13, 9,15, 0,
                          0, 0,12, 4, 0, 6,16, 0,13, 0, 0, 7, 0, 0, 0, 5,
                          0, 3, 0, 0,12, 0, 0, 0, 6, 0, 0, 4,11, 0, 0,16,
                          0, 7, 1, 0,16, 0, 5, 0,14, 0, 0, 1, 0, 0, 2, 0,
                         11, 1,15, 9, 0, 0,13, 0, 0, 2, 0, 0, 0,14, 0, 0,
                          0,14, 0, 0, 0,11, 0, 2, 0, 0,13, 3, 5, 0, 0,12];
    // drum roll..........                          
    var solution = solver.solve(puzzleNumbers);
    assert.ok(!solution, "Invalid 16x16 puzzle detected");
});

function testWith17kBank(){
    var prependZeros = function(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
    
    for(var i = 0; i < 349; i++){
        var puzzleNumber = prependZeros(i, 3);
        var url = 'res/puzzles/largeSet/PuzzleBank-' + puzzleNumber;
        (function(theUrl){
            QUnit.test( "Solver - 17K puzzle bank: " + theUrl, function(assert ) {
                var done = assert.async();
                sudoqu.PuzzleReader.getInstance().readFromUrl(theUrl,  function(puzzleArray){
                    var solver = new Solver();
                    for(var j = 0; j < puzzleArray.length; j++){
                        var solution = solver.solve(puzzleArray[j]);
                        assert.ok(solution, "puzzle bank test " + j);
                    }
                    done();
                });
            });
        }(url));
    }
}

testWith17kBank();



