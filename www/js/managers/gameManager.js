window.sudoqu = window.sudoqu || {};
/** 
 * The GameManager is a singleton that is responsible for orchestrating
 * a Sudoku game.  If offers public-facing APIs that lets other components
 * of the system request game-centric actions such as solving a puzzle or move to
 * the next one.
 * 
 * The GameManager also publishes a variety game events that convey state information
 * about the Sudoku games.  The GameManager is the sole publisher of these events
 * amd components interested in them can subscribe to them.
 * Using events rather than direct calls to other components allows for loose coupling
 * between the components of the system.
 * 
 * For more information about the game events, please see @link js/utils/gameEvents.js.
 * 
 */
window.sudoqu.GameManager = (function () {
    'use strict';

    function self() {
        _initialize();
        // **************** //
        // Public functions //
        // **************** //
        
        /**
         * Starts the GameManager and provides it with the suite of Sudoku puzzles
         * that make up the game.  This will cause the publishing of the 
         * NEW_PUZZLE_EVENT game event if the supplied puzzle array is valid or the
         * FAILURE_NO_PUZZLES_LOADED_EVENT otherwise.
         * 
         * @param {Array} puzzleArray:  Array of Sudoku puzzles that will be available to 
         *                              player.  Each element in the array is an array of
         *                              numbers representing the initial value of each cell 
         *                              in the puzzle.  
         */
        function start(puzzleArray) {
            _doStart(puzzleArray);
        }

        /**
         * Initiates the solving of the currently active Sudoku puzzle.  This will cause
         * the following game events to be published:
         *      PUZZLE_SOLVE_STARTED_EVENT:   self-explanatory
         *      PUZZLE_SOLVE_PROGRESS_EVENT:  reports progress made on computation of solution
         *      PUZZLE_SOLVE_SUCCEEDED_EVENT: published when solution is found
         *      PUZZLE_SOLVE_FAILED_EVENT:    published when no solution could be found
         * 
         */
        function solve(){
            _doSolve();
        }
        
        /**
         * Aborts the solve computation in progress. This will cause the publishing of the 
         * PUZZLE_SOLVE_CANCELLED_EVENT when cancelled.
         * 
         */
        function cancelSolve(){
            _doCancelSolve();
        }
        
        /**
         * Asks the GameManager to advance to the next puzzle.  If the GameManager
         * reaches the end of the puzzle set it got provided in the start() function
         * it will wrap around.  
         * Calling this method will cause the publishing of the NEW_PUZZLE_EVENT game event
         * which will announce the starting position of the next puzzle.
         */
        function advanceToNextPuzzle(){
            _doAdvanceToNextPuzzle();
        }

        return{
            start: start,
            solve: solve,
            cancelSolve: cancelSolve,
            advanceToNextPuzzle: advanceToNextPuzzle
        };
    }

    // ***************** //
    // Private valiables //
    // ***************** //
    var instance;
    var presentationManager;
    var puzzleNumbers;
    var solutionId;   // when defined it means that a solution is being computed.
    var currentPuzzleIndex = 0;
    var puzzleArray;

    // ***************** //
    // Private functions //
    // ***************** //
    function _initialize(){
        presentationManager = sudoqu.PresentationManager.getInstance();
    }
    
    function _doStart(_puzzleArray) {
        if(typeof _puzzleArray !== 'undefined' && _puzzleArray.length > 0){
            puzzleArray = _puzzleArray;
            currentPuzzleIndex = 0;
            puzzleNumbers = puzzleArray[currentPuzzleIndex];
            $.Events(sudoqu.GameEvents.NEW_PUZZLE_EVENT).publish(puzzleNumbers);
        }
        else{
            // invalid input - send error
            $.Events(sudoqu.GameEvents.FAILURE_NO_PUZZLES_LOADED_EVENT).publish(puzzleNumbers);
        }
    }
    
    function _doSolve(){
        // solutionId doubles as a flag indicating it a solve is in progress.  If undefined, no solve is in progress.
        if(solutionId === undefined){
            // Ask the solveManager to solve the puzzle and publish the correct event based on result.
            solutionId = sudoqu.SolveManager.getInstance().solvePuzzle(puzzleNumbers, _onPuzzleSolved, _onPuzzleProgress);
            if(solutionId){
                $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_STARTED_EVENT).publish();
            }
            else{
                // Can't happen by design - throw if it happens somehow...
                $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_FAILED_EVENT).publish();
            }
        }
    }
    
    function _doCancelSolve(){
        if(solutionId !== undefined){
            sudoqu.SolveManager.getInstance().cancelSolve(solutionId);
            solutionId = undefined;   //!!IMPORTANT - set solutionId to undefined before publishing the event
                                      //              to make sure we do not have an inconsistent state
                                      //              should the event handlers perform new operations on the 
                                      //              game mamaner;
            $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_CANCELLED_EVENT).publish();
        }
    }
    
    function _doAdvanceToNextPuzzle(){
        _doCancelSolve();  // cancel the solve in progress in case there is one.  
        // advance to next puzzle with wrap-around
        currentPuzzleIndex++;
        if(currentPuzzleIndex === puzzleArray.length){
            currentPuzzleIndex = 0;
        }
        puzzleNumbers = puzzleArray[currentPuzzleIndex];
        $.Events(sudoqu.GameEvents.NEW_PUZZLE_EVENT).publish(puzzleNumbers, currentPuzzleIndex);
    }

    function _onPuzzleSolved(solution) {
        solutionId = undefined;     // solution obtained - reset solutionId to undefined to mark that no solve is in progress
        if (solution) {
            console.debug("Solution found");
            console.debug(window.sudoqu.StringTools.buildSolutionString(solution));
            $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_SUCCEEDED_EVENT).publish(solution);
        } else {
            console.warn("Solution not found");
            $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_FAILED_EVENT).publish();
        }
    }
    
    function _onPuzzleProgress(percent){
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_PROGRESS_EVENT).publish(percent);
    }

    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if (!instance) {
                instance = self();
            }
            return instance;
        }
    };
}());
