window.sudoqu = window.sudoqu || {};

/**
 *  Small set of function that helps with generating strings related to Sudoku 
 * 
 */
(function(){
    window.sudoqu.StringTools = window.sudoqu.StringTools || {};

    /**
     * Examines a bit filed and returns a string listing the candidates
     * represented by that bitfield
     * 
     * @param {type} bitfield
     * @returns {String} string listing candidates
     */
    sudoqu.StringTools.buildCandidatesString = function(bitfield){
        var output = "";
        for(var i = 0; i < sudoqu.Constants.NUM_CELL_ROWS; i++){
            if(bitfield & (1<<i)){
                output += (i+1) + ",";
            }
        }
        return output;
    }

    /**
     * String representation of  a solution numbers.  Solution numbers are
     * arranged in the traditional Sudoku layout.
     * 
     *  5 2 7|3 1 6|4 8 9|
     *  8 9 6|5 4 2|7 3 1|
     *  3 1 4|9 8 7|5 6 2|
     *  ------------------
     *  1 7 2|4 5 3|8 9 6|
     *  6 8 9|2 7 1|3 5 4|
     *  4 5 3|6 9 8|2 1 7|
     *  ------------------
     *  9 4 1|8 2 5|6 7 3|
     *  7 6 5|1 3 4|9 2 8|
     *  2 3 8|7 6 9|1 4 5|
     *  ------------------
     * 
     * @param {type} solutionArray: array of numbers
     * @returns {String|stringTools_L17.buildSolutionString.output}
     */
    sudoqu.StringTools.buildSolutionString = function(solutionArray){
        var output = "";
        for(var i = 0; i < solutionArray.length; i++){
            output += solutionArray[i] + (i % sudoqu.Constants.NUM_CLUSTERS_PER_ROW === sudoqu.Constants.NUM_CLUSTERS_PER_ROW - 1 ? "|" : " ");
            if(i % sudoqu.Constants.NUM_CELL_COLUMNS === sudoqu.Constants.NUM_CELL_COLUMNS - 1){
                output += "\r\n";
            }
            if(i % sudoqu.Constants.NUM_CELLS_IN_CLUSTER_ROW === sudoqu.Constants.NUM_CELLS_IN_CLUSTER_ROW - 1){
                output += "------------------\r\n";
            }
        }
        return output;
    };
    
    /**
     * [NOT USED]
     * Generates a strng that shows solution values as well as possible candidates for each cell.
     * NOTE: Formatting of output string could use soem TLC.
     * 
     * @param {type} solutionArray:  array of numbers representing solution
     * @param {type} cellCandidates: array of numbers representing cell candidates expressed in a bitfield
     * @returns {String}
     */
    sudoqu.StringTools.buildPartialSolutionGridString = function(solutionArray, cellCandidates){
        var output = "";
        for(var i = 0; i < solutionArray.length; i++){
            var value = "";
            if(solutionArray[i]){
                value = solutionArray[i];
            }
            else{
                value = sudoqu.StringTools.buildCandidatesString(cellCandidates[i]);
            }
            output += value + (i % sudoqu.Constants.NUM_CLUSTERS_PER_ROW === sudoqu.Constants.NUM_CLUSTERS_PER_ROW - 1 ? "|" : " ");
            if(i % sudoqu.Constants.NUM_CELL_COLUMNS === sudoqu.Constants.NUM_CELL_COLUMNS - 1){
                output += "\r\n";
            }
            if(i % sudoqu.Constants.NUM_CELLS_IN_CLUSTER_ROW === sudoqu.Constants.NUM_CELLS_IN_CLUSTER_ROW - 1){
                output += "------------------\r\n";
            }
        }
        return output;
    }

}());

