/**
 * This reader is responsible for obtaining Sudoku puzzle layouts from a string passed 
 * as a parameter or obtained via a URL.  Each line of the string describes a Sudoku
 * puzzle epressed as a sequence of 81 characters, each one describing a Sudoku cell.
 * 
 * If the character is an 'X', 'x' or a '.', it means that the cell is blank.  If the character 
 * is a number between 1 and 9, this represents the value of that cell. 
 * Example:
 * The following string: 
 * '..9..54...5.......2.83..9.....8.6.3.....5...782.4......71.9.......24.6.3.....3..9'
 * would be used to describe the following Sudoku puzzle:
 * -------------------------------------
 * |         9 |         5 | 4         |
 * |   +   +   +   +   +   +   +   +   |
 * |     5     |           |           |
 * |   +   +   +   +   +   +   +   +   |
 * | 2       8 | 3         | 9         |
 * |---+---+---+---+---+---+---+---+---|
 * |           | 8       6 |     3     |
 * |   +   +   +   +   +   +   +   +   |
 * |           |     5     |         7 |
 * |   +   +   +   +   +   +   +   +   |
 * | 8   2     | 4         |           |
 * |---+---+---+---+---+---+---+---+---|
 * |     7   1 |     9     |           |
 * |   +   +   +   +   +   +   +   +   |
 * |           | 2   4     | 6       3 |
 * |   +   +   +   +   +   +   +   +   |
 * |           |         3 |         9 |
 * -------------------------------------
 * 
 * The puzzle reader converts each valid Sudoku puzzle it finds in the string
 * into an array of 81 Numbers.  The result of the processing is returned via a callback
 * that accepts a single parameter which is an array of arrays of number.  Each
 * element in the outer array represents one Sudoku puzzle.
 * 
 */
window.sudoqu = window.sudoqu || {};

window.sudoqu.PuzzleReader = (function () {
    'use strict';

    function self() {
        // **************** //
        // Public functions //
        // **************** //

        /**
         * 
         * @param {string} url - URL of text file containing puzzle layout 
         * @param {function} callback - function to be called with array.  This array will contain one array of Numbers for each puzzle read.
         *                              If no valid puzzle is found, an empty array is returned
         * @returns - nothing
         * @ throws - Error() if null or undefined callback or URL
         */
        function readFromUrl(url, callback) {
            if(!url || !callback || typeof url !== "string"){
                throw new Error("PuzzleReader::readFromUrl - invalid parameters");
            }
            _doReadFromUrl(url, callback);
        }

        /**
         * 
         * @param {string} puzzleString - string containing one or more Sudoku puzzles serapated by LF or CRLF characters.
         * @param {function} callback - function to be called with array.  This array will contain one array of Numbers for each puzzle read.
         *                              If no valid puzzle is found, an empty array is returned
         * @returns - nothing
         * @ throws - Error() if null or undefined callback or URL
         */
        function readFromString(puzzleString, callback) {
            if(!puzzleString || !callback || typeof puzzleString !== "string"){
                throw new Error("PuzzleReader::readFromString - invalid parameters");
            }
            _doReadFromString(puzzleString, callback);
        }

        return{
            readFromUrl: readFromUrl,
            readFromString: readFromString
        };
    }

    // ***************** //
    // Private valiables //
    // ***************** //
    var instance;

    // ***************** //
    // Private functions //
    // ***************** //
    function _doReadFromUrl(url, callback) {
        $.ajax({
            url: url,
            dataType: "text",
            success: function (rawPuzzleData) {
                var processedArray = _processRawPuzzleData(rawPuzzleData.trim());
                callback(processedArray);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error("failed to obtain puzzle info for URL " + url + " => " + textStatus + "; " + errorThrown);
                callback([]);
            }
        });
    }


    function _doReadFromString(puzzleString, callback) {
        var processedArray = _processRawPuzzleData(puzzleString);
        callback(processedArray);
    }

    /**
     * 
     * @param {string} rawPuzzleData: string representing Sudoku puzzles in textual form as described 
     *                 in comments at the top of the file.
     * @returns {Array}
     */
    function _processRawPuzzleData(rawPuzzleData) {
        if (!rawPuzzleData) {
            return [];
        }
        
        var arrayOfPuzzles = [];

        var puzzles = rawPuzzleData.split('\n');  // separate each line - each line describes on Sudoku puzzle
        for(var puzzleIndex = 0; puzzleIndex < puzzles.length; puzzleIndex++){
            var numberArray = [];
            var rawPuzzleData = puzzles[puzzleIndex].trim();
            // verify that the layout data has the right number of characters in it.
            if (rawPuzzleData.length !== sudoqu.Constants.TOTAL_NUM_CELLS) {
                console.error("_processRawPuzzleData: Invalid number of characters: " + rawPuzzleData.length);
                continue;
            }

            for (var i = 0; i < rawPuzzleData.length; i++) {
                var character = rawPuzzleData.charAt(i);
                if (character === 'X' || character === 'x' || character === '.') {
                    numberArray.push(0);
                } else {
                    var number = parseInt(rawPuzzleData.charAt(i));
                    if (isNaN(number)) {
                        console.warn("PuzzleReader found invalid character value for puzzleIndex; " + puzzleIndex + " charIndex: " + i + "; value = " +  character);
                        break;   // neither an empty cell or a number, invalid puzzle, abort this one
                    }
                    numberArray.push(number);
                }
            }
            if(i === rawPuzzleData.length){
                // we went all the way, puzzle is valid - add it to the array of puzzles
                arrayOfPuzzles.push(numberArray);
            }
        }
        return arrayOfPuzzles;
    }

    return {
        // Get the Singleton instance if one e.ists
        // or create one if it doesn't
        getInstance: function () {
            if (!instance) {
                instance = self();
            }
            return instance;
        }
    };
}());
