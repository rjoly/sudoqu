SudoQ in a sentence
===================
SudoQ is an iOS application that implements a rudimentary Sudoku game.

Brief description of the implementation
=======================================
SudoQ is a responsive Cordova-based hybrid app.  The game logic and the solver are implemented 
in JavaScript with JQuery and the presentation layer is implemented using HTML5 and CSS3.   
The game can run on any iOS device that has iOS version 9 or above.  The game can 
run inside a browser as well and has been verified to work with the Google Chrome browser.

The app is broken down into a small set of components.  Here they are in a nutshell:
 - SudoquApp           -> Commissions the reading of a puzzle description file and starts the game
 - PuzzleReader        -> Reads text-based puzzles from string or URL and massages them into the binary
                          format that the game logic expects.
 - GameManager         -> Provides public-facing API to perform operations on a game and publishes
                          game events.
 - PresentationManager -> Responsible for everything the user sees.  Reacts to user input and game events.   
 - SolveManager        -> Simple abstraction layer for Solver threads                          
 - Solver              -> Generic, stand-alone Sudoku solver that can handle 4x4, 9x9, 16x16 or 25x25 puzzles.

Test automation is performed using QUnit and unit tests are focused on the primary deliverable of the 
coding assignment which is the Solver logic.

Repository information
======================
The source code is hosted inside a public BitBucket repository at https://bitbucket.org/rjoly/sudoqu.

The HTML5, CSS3, JavaScript as well as game assets that make up the Cordova application can 
be found under the 'www' directory.  The corresponding Xcode project for building and 
running the game using Xcode can be found under the directory 'platforms/ios'.

Running the game
================
There are many ways that you can actually run the game.

From a browser:
 1- A public version of the game is hosted at http://qlik.tremblayjoly.com:9080/qlik/sudoQ/index.html
 2- You can serve the content of the www directory on a local web server and point your browser to it.

From Xcode:
 Open the Xcode project found under the directory 'platforms/ios' and build & run the 'SudoQ' from within Xcode.  Be
 sure to select the 'SudoQ' scheme.

Running the automated tests
===========================
Automated tests can be run from the browser by accessing the URL http://<webServerLocation>/<path to sudoQ siteroot>/test/test.html.
To run automated tests on the public version of the app, you can go to 
http://qlik.tremblayjoly.com:9080/qlik/sudoQ/test/test.html

Providing your own set of puzzles
=================================
There are three ways to provide your own puzzles to the sudoQ app but before we dive into those, let's first
discuss the format. A puzzle is described as an 81 character-long line that specifies the start value of each
cell in the puzzle.  A value of '.', 'X' or 'x' designates a cell whose value is unknown and a value between
'1' and '9' designates the cell's initial value.  A puzzle definition file can have multiple puzzle definition
lines. See http://magictour.free.fr/top95 for example.

The default set of Sudoku puzzles offered by the app are found in the file 'res/other/defaultPuzzles.txt'
under the 'www/' directory of the repository but there are three ways of specifying alternate puzzles:
     #1 - edit res/other/defaultPuzzles.txt to include your puzzles and rebuild the app.
     #2 - when running in a browser, you can instruct the app to use an alternate puzzle definition file
          however, because of CORS, this file needs to be located on the same server as the SudoQ app. The 
          URL of the file is specified via a query string 'url' parameter.  For example,  if the app is 
	  accessed using address http://mysudoqserver.com/somepath/index.html?url=res/other/myownpuzzles.txt,
          the res/other/myownpuzzles.txt will be used as the puzzle definition file.
     #3 - when running in a browser, you can instruct the app to use a specific puzzle by specifying it in the URL
          via the puzzle query string parameter.
          Example: http://mysudoqserver.com/somepath/index.html?puzzle=.1.9.8.6392..5...146..7......95..6.8.5.4.3.1.8.1..63......4..366...3..8237.8.2.9.     

How to play the game
====================
The game offer 95 very challenging Sudoku puzzles that the player can try to solve.  
A player can tap a cell to bring up a key pad and enter its value.  The key pad positioning
varies based on the orientation of the device - try both to see which one feels more 
comfortable for you.  Once you think you reached the solution or give up, tap the 'Solve' 
button to reveal the solution.  Any user-supplied answers that are wrong will appear in red.  
Tap the 'Next' button to advance to the next puzzle.

Other information
=================
 - http://magictour.free.fr/ has been a great source of difficult Sudoku puzzles.
 - Logo and splash screen have been created using the amazing Blender 3D editor (blender.org)
   and post-processing has been done using Gimp.


