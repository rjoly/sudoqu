window.sudoqu = window.sudoqu || {};

/**
 * Function that defines constants that are used to describe the key dimensions 
 * of the 9x9 Sudoku puzzle.  In order to make sense of these constants, it is important 
 * to understand the terminology being used:
 *      CELL: The most fundamental element in the Sudoku puzzle.  It is number square
 *      CLUSTER: An 3 by 3 array of cells within which all cell values must be unique.
 *               A traditional 9x9 Sudoku puzzle is made up of 3 rows of  3
 *               clusters each.
 *               
 * Note that the UI of the app imposes 9x9 Sudoku puzzles but the Solver can solve puzzles
 * up to 25x25.  Becasue of that, the Solver uses its own set of constants altogether which
 * are not restricted to 9x9.
 */
(function(){
    window.sudoqu.Constants = window.sudoqu.Constants || {};
    sudoqu.Constants.TOTAL_NUM_CELLS = 81;        
    sudoqu.Constants.NUM_CELL_ROWS = 9;
    sudoqu.Constants.NUM_CELL_COLUMNS = 9;
    sudoqu.Constants.NUM_CLUSTERS_PER_ROW = 3;
    sudoqu.Constants.NUM_CELLS_IN_CLUSTER_ROW = 27;
}());
