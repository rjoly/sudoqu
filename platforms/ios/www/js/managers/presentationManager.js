window.sudoqu = window.sudoqu || {};

/**
 * The PresentationManager is a singleton that manages everything that has to do
 * with the UI presentation and it also handles the player interactions.  Its main
 * purpose is to handle to the player's input and to update the presentation based
 * on the incoming game events.
 * 
 */
window.sudoqu.PresentationManager = (function () {
    'use strict';

    function self(){
        _initialize();
        // **************** //
        // Public functions //
        // **************** //
            
        // No public interface...
        
        return{
        };
    }
    
    // ***************** //
    // Private valiables //
    // ***************** //
    var instance;
    var puzzleNumbersArray;
    var $progressBar;
    
    // ***************** //
    // Private functions //
    // ***************** //
    function _initialize(){
        // Initialize progress bar.  Progress bar comes from a 3rd party
        // jQuery plugin and the way it is implemented, the element that is 
        // going to contain it has to be visible when the progress bar is initialized.
        // For that reason, the progress bar starts off visible and is quickly hidden
        // immediately after the initialization.
        $progressBar = $('#progressBar');
        $progressBar.progressBar({showPercent: false, height: '20px'});
        _hideProgressBar(); // progress bar init done, we can hide it now.
        
        // Install click handlers.
        // TODO: Click handlers have slow response time on mobile platforms.
        //       Improve response time by relying on touch events instead.
        $('#solveCancelButton').click(_onSolveCancelButtonClicked);
        $('#clearButton').click(_onClearButtonClicked);
        $('#nextButton').click(_onNextButtonClicked);
        $('.keyPadDigit').click(_onKeypadButtonClicked);
        $('.sudokuCell').click(_onSudokuCellClicked);
        $('body').click(_onOutsideClicked);
        
        // Install game event listeners
        $.Events(sudoqu.GameEvents.NEW_PUZZLE_EVENT).subscribe(_onNewPuzzleEvent);
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_STARTED_EVENT).subscribe(_onSolveStartedEvent);
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_CANCELLED_EVENT).subscribe(_onSolveCancelledEvent);
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_SUCCEEDED_EVENT).subscribe(_onSolveSucceededEvent);
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_FAILED_EVENT).subscribe(_onSolveFailedEvent);
        $.Events(sudoqu.GameEvents.PUZZLE_SOLVE_PROGRESS_EVENT).subscribe(_onSolveProgressEvent);
        $.Events(sudoqu.GameEvents.FAILURE_NO_PUZZLES_LOADED_EVENT).subscribe(_onFailureToLoadPuzzlesEvent);
    }
    
    /* ******************* */
    /* GameEvent listeners */
    /* ******************* */
    function _onNewPuzzleEvent(_puzzleNumbersArray){
        puzzleNumbersArray = _puzzleNumbersArray;
        _showPuzzle(puzzleNumbersArray);
    }

    function _onSolveStartedEvent(){
        _resetProgressBar();
        _showProgressBar();
        _showCancelButton();
    }

    function _onSolveCancelledEvent(){
        // The solve has been cancelled.  Repaint the puzzle to its original form 
        // and turn the middle button back into a 'Solve' button.
        _showPuzzle(puzzleNumbersArray);
        _resetProgressBar();
        _hideProgressBar();
        _showSolveButton();
    }        
    
    function _onSolveSucceededEvent(solution){
        _showSolution(solution);
        _hideProgressBar();
        _showSolveButton();
    }
    
    function _onSolveFailedEvent(){
        noty({      // noty is a cool notification banner plugin for  jquery (http://ned.im/noty/#/about)
            text: 'No solution could be found for this puzzle',
            timeout: 3000,
            type: "error",
            animation: {
                open: {height: 'toggle'}, 
                close: {height: 'toggle'}, 
                easing: 'swing', 
                speed: 200
            }
        });
        _hideProgressBar();
        _showSolveButton();
    }
    
    function _onSolveProgressEvent(percent){
        percent = Math.round(percent * 100);
        $progressBar.changePercent(percent);
    }
    
    function _onFailureToLoadPuzzlesEvent(){
        noty({
            text: 'No valid puzzles loaded',
            timeout: 3000,
            type: "error",
            animation: {
                open: {height: 'toggle'}, 
                close: {height: 'toggle'}, 
                easing: 'swing', 
                speed: 200
            }
        });
        $('#solveCancelButton').hide();
    }
            
    /* ************** */
    /* Click Handlers */
    /* ************** */            
    function _onSolveCancelButtonClicked(event){
        // This button has dual state and is the 'Solve'
        // button when no puzzle is being solved and is
        // the 'Cancel' button when a solve is in progress.
        // We use the solveInProgress class on the element
        // to track its state.
        var $button = $(this);
        if($button.hasClass('solveInProgress')){
            _onCancelButtonClicked();
        }
        else{
            _onSolveButtonClicked();
        }
        event.preventDefault();
        event.stopPropagation();
    } 

    function _onSolveButtonClicked(){
        _hideKeypad();
        sudoqu.GameManager.getInstance().solve();
    }        

    function _onCancelButtonClicked(){
        _hideKeypad();
        sudoqu.GameManager.getInstance().cancelSolve();
    }
    
    // A keypad button is clicked, apply the clicked value to the 
    // selected Sudoku cell if that cell does not have an initial
    // value set by the puzzle.
    // NOTE: the keypad does not have a delete function.  the way to 
    //       delete a mumber is to set the same value again.  I.e. 
    //       if a cell has a value of 3 and the player wants to clear
    //       it, he/she must select it and assign it the value of 3 again.
    //       Obviously, this is a bit of a hack that would not stay if that
    //       were a real Suduku game... :)
    function _onKeypadButtonClicked(event){
        var $selectedSudokuCell = $('.sudokuCell.selected');
        var $selectedSudokuCellNumber = $('.number', $selectedSudokuCell);
        if($selectedSudokuCell.length > 0){
            var keypadValue =  $(this).data('value');
            if(keypadValue !== undefined){
                var currentCellValue = $selectedSudokuCellNumber.text();
                if(Number(currentCellValue) === keypadValue){       // new value is the same as previous - this clears it.
                    $selectedSudokuCell.removeClass('selected');
                    $selectedSudokuCellNumber.removeClass('userEnteredValue').text("");
                }
                else{
                    $selectedSudokuCell.removeClass('selected');
                    $selectedSudokuCellNumber.addClass('userEnteredValue').text(keypadValue);
                }
            }
        }
        _hideKeypad();
        event.preventDefault();
        event.stopPropagation();
    }
    
    function _onClearButtonClicked(event){
        _showPuzzle(puzzleNumbersArray);
        event.preventDefault();
        event.stopPropagation();
    }
    
    function _onNextButtonClicked(event){
        sudoqu.GameManager.getInstance().cancelSolve();  // cancel current solve in case it was active before advancing to next one.
        sudoqu.GameManager.getInstance().advanceToNextPuzzle();
        event.preventDefault();
        event.stopPropagation();
    }

    // toggle selection state for cell and show or hide keypad accordingly.
    function _onSudokuCellClicked(event){
        var $selectedCell = $(this);
        if($selectedCell.hasClass('selected')){
            // cell is already selected - simply unselect it
            $selectedCell.removeClass('selected');
            _hideKeypad();
        }
        else{
            if(!$selectedCell.find('.number').hasClass('initialValue')){   // do not let the user overwrite the puzzle's intial value
                $('.sudokuCell.selected').removeClass('selected');   // unselect previously-selected cell.
                $selectedCell.addClass('selected');
                _showKeypad();
            }
        }
        event.preventDefault();
        event.stopPropagation();
    }

    // dismiss keypad is user clicks anywhere outside active UI elements.
    function _onOutsideClicked(event){
        _hideKeypad();
        event.preventDefault();
        event.stopPropagation();
    }
    
    /* ***************** */
    /* Supoort functions */
    /* ***************** */     
    function _showPuzzle(puzzleNumbersArray){
        _hideKeypad();      // make sure keypad is not hiding puzzle to be shown
        $('.sudokuCell').removeClass('selected error');                                 // return all cells to their wirgin state...
        $('#sudokuGrid .number').text("").removeClass('initialValue userEnteredValue'); // ... and return all cell numers to their wirgin state too
        for(var i = 0; i < puzzleNumbersArray.length; i++){
            if(puzzleNumbersArray[i] !== 0){
                $('#cell' + i + ' .number').addClass('initialValue').text(puzzleNumbersArray[i]);
            }
        }
    }      
    
    // called when solution is received via event.  Repaint the cells with solution
    // and mark the user-provided values that do not match the solution with the 
    // 'error' class.
    function _showSolution(solution){
        for(var i = 0; i < solution.length; i++){
            if(solution[i] !== 0){
                var $cellNumber = $('#cell' + i + ' .number');
                if($cellNumber.hasClass('userEnteredValue')){
                    var userEnteredValue = $cellNumber.text();
                    if(userEnteredValue){
                        if(Number(userEnteredValue) !== solution[i]){
                            $cellNumber.parent().addClass('error');
                        }
                        else{
                            $cellNumber.parent().removeClass('error');
                        }
                    }
                    $cellNumber.removeClass('userEnteredValue');
                }
                else{
                    $cellNumber.parent().removeClass('error');
                }
                $('#cell' + i + ' .number').text(solution[i]);
            }
        }
    }
    
    function _showSolveButton(){
        var $button = $('#solveCancelButton');
        $button.removeClass('solveInProgress').text("Solve");
    }

    function _showCancelButton(){
        var $button = $('#solveCancelButton');
        $button.addClass('solveInProgress').text("Cancel");
    }

    function _resetProgressBar(){
        $progressBar.changePercent(0);
    }
    
    function _showProgressBar(){
        $('#progressBar').removeClass('hidden');
    }
    
    function _hideProgressBar(){
        $('#progressBar').addClass('hidden');
    }
    
    function _showKeypad(){
        $('#puzzleArea').addClass('keyPadMode');
    }
    
    function _hideKeypad(){
        $('#puzzleArea').removeClass('keyPadMode');
        $('.sudokuCell.selected').removeClass('selected');
    }

    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = self();
            }
            return instance;
        }
    };
}());
