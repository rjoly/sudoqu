window.sudoqu = window.sudoqu || {};

/**
 * Solving harder Sudoku puzzles can take several seconds.  Given that, it is
 * essential that we offload the solution computation to a Worker thread.  Failure
 * to do so would result in the UI freezing for the duration of the computation
 * which is not acceptable.  The SolveManager is rersponsible for providing a
 * simple interface through which other components of the app can request Sudoku
 * solutions to the puzzles they submit.   Under the hood, the SolveManager 
 * handles the task of starting and stopping 'solve' threads and report solve solutions
 * back to requester via callbacks.
 * 
 * The Solvemanager is designed to handle up to 4 concurrent solves but, as it stands,
 * the app only uses one at the time.
 */
window.sudoqu.SolveManager = (function () {
    'use strict';
    
    var MAX_NUMBER_OF_WORKERS = 4;

    function self(){
        // **************** //
        // Public functions //
        // **************** //
        
        /**
         * 
         * @param {Array} puzzleNumbersArray - array of ints that represent the pre-determined puzzle values, i.e.
         *                                     the cells that are revealed.  A value of 0 means the cell value is unknown.
         * @param {function} resultCallback - callback accepts a single parameter which will be the solution array of Numbers if a solution 
         *                                    is reached or null if no solution reached indicating that the sudoku puzzle was malformed.
         *                                    The array contains integers ordered from left-to-right and top-to-bottom.
         * @param {function} progressCallback [optional] - If supplied, will be called with a completion percentage.  This
         *                                                 is not a true representation of the progress since the algorithm
                                                           can sometimes backtrack on a solution if it reaches an invalid state therefore
         *                                                 lowering progress.  The percentage reported in the progress callback is the high water mark.
         * @returns: A unique solution Id or null of no more request can be accepted. 
         */
        function solvePuzzle(puzzleNumbersArray, resultCallback, progressCallback){
            return _doSolvePuzzle(puzzleNumbersArray, resultCallback, progressCallback);
        }

        /**
         * 
         * @param {Number} solutionId: identifies the solution to cancel
         */
        function cancelSolve(solutionId){
            _doCancelSolve(solutionId);
        }
        
        return{
            solvePuzzle: solvePuzzle,
            cancelSolve: cancelSolve
        };
    }
    
    // ***************** //
    // Private valiables //
    // ***************** //
    var instance;
    var nextSolutionId = 0;
    var solveDescriptors = {};
    var activeThreadCount = 0;
    
    // ***************** //
    // Private functions //
    // ***************** //

    // strart new worker thread if inputs check out and we still have room.
    function _doSolvePuzzle(puzzleNumbersArray, resultCallback, progressCallback){
        if(activeThreadCount >= MAX_NUMBER_OF_WORKERS || !puzzleNumbersArray || !resultCallback){
            return null;
        }
        var solutionIdToReturn = _getNextSolutionId();
        activeThreadCount++;
        var solverThread = new Worker('js/solver/solverThread.js');
        solveDescriptors[solutionIdToReturn] = { thread:           solverThread,
                                                 resultsCallback:  resultCallback,
                                                 progressCallback: progressCallback};
                                             
        // Register a messsge handler that will listen for messages from thread
        solverThread.addEventListener('message', _onThreadMessage, false);//                                                  
        // Inform the thread of its Id.  All subsequent messages coming from it will carry that id.
        solverThread.postMessage({'cmd': 'setSolutionId', 'id': solutionIdToReturn});
        // Ask the thread to solve the puzzle
        solverThread.postMessage({'cmd': 'solve', 'puzzle': puzzleNumbersArray});
        return solutionIdToReturn;
    }
    
    function _doCancelSolve(solutionId){
        var solutionDescriptor = solveDescriptors[solutionId];
        if(solutionDescriptor){
            // kill the thread and clean up.
            solutionDescriptor.thread.terminate();
            activeThreadCount--;
            delete solveDescriptors[solutionId];
        }

    }
    
    function _getNextSolutionId(){
        return ++nextSolutionId;
    }

    // Interface with Worker threads is through messages only.  
    // This function is the one that handles messages posted by the thread
    // and converts them into calls on the registered callbacks.
    function _onThreadMessage(event) {
        var data = event.data;
        switch (data.cmd) {
            case 'solveResult':
                var solutionId = data.id;
                var solutionDescriptor = solveDescriptors[solutionId];
                if (solutionDescriptor) {
                    activeThreadCount--;
                    solutionDescriptor.resultsCallback(data.solution);
                    delete solveDescriptors[solutionId];
                } else {
                    console.error("Cannot find descrptior for solution " + solutionId);
                }
                break;
            case 'progress':
                var solutionId = data.id;
                var solutionDescriptor = solveDescriptors[solutionId];
                if (solutionDescriptor && solutionDescriptor.progressCallback) {
                    solutionDescriptor.progressCallback(data.percent);
                } else {
                    console.error("Cannot find descrptior for solution " + solutionId);
                }
                break;
            default:
                console.error("SolverThread: unknoen command: " + data.cmd);
        }
    }
    
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = self();
            }
            return instance;
        }
    };
}());
