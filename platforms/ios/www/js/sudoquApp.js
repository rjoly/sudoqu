window.sudoqu = window.sudoqu || {};

/**
 * This singleton represent the SudoQ app.  It is responsible for initiating the reading
 * of the puzzles via the PuzzleReader component and feed them to the GameManager so that 
 * the game can start.
 * 
 * By default, the Sudoku puzzles offered by the app are found in the file 'res/other/defaultPuzzles.txt'
 * under the www directory but there are three ways of specifying alternate puzzles:
 *      #1 - edit res/other/defaultPuzzles.txt to include your puzzles;
 *      #2 - when running from a browser, you can instruct the app to use an alternate puzzle definition file
 *           however, becasue of CORS, this file needs to be hosted on the server as the main app.  The 
 *           alternate puzzle definition file is specified via a query string 'url' parameter.  For example,
 *           if the app is accessed via the url http://mysudoqserver.com/somepath/index.html?url=res/other/myownpuzzles.txt,
 *           the myownpuzzles.txt will be used as the puzzle definition file.
 *      #3 - when running from a browser, you can instruct the app to use a specific puzzle by specifying it in the URL.
 *           Example: http://mysudoqserver.com/somepath/index.html?puzzle=.1.9.8.6392..5...146..7......95..6.8.5.4.3.1.8.1..63......4..366...3..8237.8.2.9.     
 * 
 */
window.sudoqu.SudoquApp = (function () {
    'use strict';

    function self(){
        // **************** //
        // Public functions //
        // **************** //
        function start(){
            _doStart();
        }

        return{
            start: start
        };
    }
    
    // ***************** //
    // Private valiables //
    // ***************** //
    var instance;
    
    // ***************** //
    // Private functions //
    // ***************** //
    
    function _doStart(){
        // ...
        console.debug("Hello world");

        // check if URL contains a puzzle query string.  If present, try to use it to seed the puzzle.
        // E.g. http://www.sudoqu.com/index.html?puzzle=.1.9.8.6392..5...146..7......95..6.8.5.4.3.1.8.1..63......4..366...3..8237.8.2.9.
        var queryStringParams = _getQueryStringParameters();
        if(queryStringParams["puzzle"]){
            sudoqu.PuzzleReader.getInstance().readFromString(queryStringParams["puzzle"], _onLayoutLoaded);
        }
        else{
            // puzzle query string not found, we're going to load puzzle(s) from a URL.  Check if there
            // is a URL query string to override the detail.
//            var url = 'res/other/defaultPuzzle.txt';
            var url = 'res/other/defaultPuzzles.txt';
            if(queryStringParams["url"]){
                url = queryStringParams["url"];
            }
            sudoqu.PuzzleReader.getInstance().readFromUrl(url,  _onLayoutLoaded);
        }
        return;
    }
 
    
    function _onLayoutLoaded(puzzleArray){
        sudoqu.GameManager.getInstance().start(puzzleArray);
        return;
    }
    
    function _getQueryStringParameters(){
        var qsParams = document.location.search.replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0]; // Stolen from https://css-tricks.com/snippets/jquery/get-query-params-object/
        return qsParams;
    }
    
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = self();
            }
            return instance;
        }
    };
}());
