
/**
 * The solver is built to be completely stand-alone.  It does not rely on any other component
 * of the app and is completely self-sufficient and is designed to be used as the solving engine
 * for Sudoku-based games inplemented in JavaScript.
 * 
 * The Solver is flexible and can solve 4x4, 9x9, 16x16 and 25x25 Sudoku puzzles.  The Solver 
 * dynamically generates its set of constants and lookup tables based on the input puzzle's length.
 * 
 * the main task of the solver is to evaluate candidates for a given cell.  The possible
 * candidates of a given cell are all the possible values that are not found in the 
 * row, column or cluster that the cell is in.  To maximize efficiency, the set of candidates for rows,
 * columns, clusters and cells are tracked using bitfields.  Using bitfields make it possible
 * to compute cell candidates with simple bitwise operators.  Although not as striking
 * in JavaScript, bitwise operations are generally among the most efficient operations a CPU can perform.
 * 
 * In a 9x9 Sudoku puzzle, each cell can have 1 of 9 possible values.  The bitfield
 * dedicates one bit for each value as follows:
 * 
 *       Bit position in bitfield     876543210      
 *                                    ^^^^^^^^^
 * Bit represents value 9 ____________|||||||||______________ Bit represents value 1
 *                                     |||||||
 * Bit represents value 8 _____________|||||||_______________ Bit represents value 2                   
 *                                      |||||
 * Bit represents value 7 ______________|||||________________ Bit represents value 3                   
 *                                       |||
 * Bit represents value 6 _______________|||_________________ Bit represents value 4                   
 *                                        |
 * Bit represents value 5 ________________|
 * 
 * When a bit is set, it means that its corresponding value is a possible candidate for the cell.
 * For example, if a cell has a bitfield of decimal value 112 or 001110000 it means that 
 * values 5, 6 and 7 are possible candidate values for that cell.  
 * 
 * The solver also uses bitfields for rows, columns and clusters to track the values assigned to the cells within them.
 * Here, a bit set to 1 indicates that the corresponding value has not been taken and a bit cleared (==0) indicates that 
 * the value is already assigned to one of the cells in it.
 * 
 * As an example, let's assume that cellX is in rowA, columB and clusterC. 
 * rowA has values 2, 4, 8 already assigned to cells within it.           Its resulting bitfield will therefore be 101110101
 * columnB has values 1, 2, 5 and 8 already assigned to cells within it.  Its resulting bitfield will therefore be 101101100
 * clusterC has values 3 and 9 already assigned to cells within it.       Its resulting bitfield will therefore be 011111011
 * 
 * With this data representation, it is possible to very efficiently compute the possible candidates for cellX by performing
 * a bitwise AND operation between the bitfields of rowA, columnB and clusterC.
 *          101110101       (rowA's bitfield)
 *      AND 101101100       (columnB's bitfield)
 *      AND 011111011       (clusterC's bitfield)
 *          ---------
 *       =  001100000  <- The bits for cell values 6 and 7 are set indicating that the only possible values for cellX are 6 and 7.
 *       
 * A cell is solved when only one candidate remains.  When the algorithm reaches a point where an unsolved cell has
 * no remaining candidates (i.e. its candidates bitfield is 0), this means that the solution path is invalid.
 * 
 * Note that the design choice of using bitfields to track candidates puts a hard limit on the maximum size of Sudoku puzzle
 * that it can solve.  Javascript uses 32-bit integers to perform bitwise operations meaning that 25x25 is the largest Sudoku
 * puzzles it can solve.  This was deemed an acceptable limitation since Sudoku puzzle games almost never propose puzzles beyond 16x16.
 *       
 */
Solver = (function () {
    'use strict';

    var MAX_NUMBER_OF_CELL_VALUES = 25;  // we allow up to 25x25 puzzles
    
    function InvalidSolutiontException(sMessage) {
        this.name = "IllegalArgumentException";
        this.message = sMessage;
        this.stack = (new Error()).stack;
    }
    InvalidSolutiontException.prototype = Object.create(Error.prototype);
    InvalidSolutiontException.prototype.constructor = InvalidSolutiontException;    

    function theSolver() {
    }

    /**
     * Attempts to solve a puzzle.
     * 
     * @param {Array} puzzleNumbersArray - array of Numbers that contains the initial values of the Sudoku puzzle cells. A
     *                                     value of 0 indicates a cell with an unknown value.
     * @param {function} progressCallback - callback accepts a single parameter which represents the percentage of the puzzle
     *                                      being solved.  The callback is called every time the solution percentage sets a new
     *                                      high water mark.
     *                                      
     * @return {Array} solution array if a solution reached or null if no solution reached indicating that the sudoku puzzle was malformed.
     *                 The solution array contains Numbers representing the value of each cell in the solution.  Cell values are  ordered 
     *                 from left-to-right and top-to-bottom.
     */                                
    theSolver.prototype.solve = function (puzzleNumbersArray, progressCallback) {
        if(!puzzleNumbersArray){
            return null;
        }
        
        // initialize the constants based o nthe input puzzle size.
        if(!this._initializeConstants(puzzleNumbersArray.length)){
            return null;
        }
        this._initializeCellIndexLookupTables();                   
       
       // variable initialization
        this.startTimestamp = new Date().getTime();
        this.progressCallback = progressCallback;
        this.solvedCellHighWaterMark = 0;      // used to throttle down rate at which progress notifications are generated
        
        // kick off the actual solve process.
        var solution = this._doSolve(puzzleNumbersArray);
        if(solution){
            // We obtained a solution - validate it against the puzzle.  
            // If the verification fails, it points to a bug in the solver.
            if(!this._verifySolutionCompliance(solution, puzzleNumbersArray)){
                console.error("Solver: solution verification failed.  Indicates a bug in the solver :(");
                // If we have access to StringTools, use it to output the solution and puzzle
                if(typeof sudoqu !== 'undefined' && typeof sudoqu.StringTools !== 'undefined'){
                    console.error("        puzzle:   " + sudoqu.StringTools.buildSolutionString(puzzleNumbersArray));
                    console.error("        solution: " + sudoqu.StringTools.buildSolutionString(solution));
                }
                else{
                    // StringTools not available  - do rough output of sultion and puzzle
                    var solutionString = "Solution: ";
                    var puzzleString   = "Puzzle:   ";
                    for(var index = 0; index < solution.length; index++){
                        solutionString += solution[index];
                        puzzleString += puzzleNumbersArray[index];
                    }
                    console.error("        puzzle:   " + puzzleString);
                    console.error("        solution: " + solutionString);
                }
                return null;
            }
        }
        return solution;
    }
    
    /**
     * This function is the workhorse of the solver.  It first tries to solve the Sudoku puzzle by
     * eliminating candidates on each cell.  When no more candidates can be eliminated and the puzzle
     * is not yet solved, _doSolve() will call itself recursively to find the brute-force solution by 
     * guessing the value of unsolved cells and see if the guess leads to a valid solution.
     * 
     * @param {type} puzzleNumbersArray -  array of Numbers that contains the initial values of the Sudoku puzzle cells. A
     *                                     value of 0 indicates a cell with an unknown value
     * @returns: Solution array if solution reached or null if no solution reached.
     */
    theSolver.prototype._doSolve = function (puzzleNumbersArray) {
        try{
            /* ***************************************** */
            /*                                           */           
            /* STEP 1 = Add pre-solved cells to sulution */
            /*                                           */           
            /* ***************************************** */
            var solutionContext = this._createNewSolutionContext();
            // Go through the puzzle and add the pre-solved cells into the solution.
            for(var i = 0; i < puzzleNumbersArray.length; i++){
                if(puzzleNumbersArray[i] !== 0){        // !== 0 meams that the cell value is known - add it to solution
                    if(this._addToSolution(i, puzzleNumbersArray[i], solutionContext)){
                        // _addSolution() returned true indicating that a solution was reached, return it.
                        return solutionContext.solution;
                    }
                }
            }

            /* **************************************************************************** */
            /*                                                                              */           
            /* STEP 2 = Look for cells that are the only ones to have a given candidate     */
            /* for their row, cell or column and add them to the solution.                  */
            /* As cells are added to the solution, candidates get eliminated from remaining */
            /* cells.  Keep going through the cells looking for solved ones until we no     */
            /* longer find any.                                                             */ 
            /*                                                                              */           
            /* **************************************************************************** */
            var preSolvedCount;
            var postSolvedCount;
            do{
                preSolvedCount = solutionContext.solvedCellCount;
                if(this._solveUniqueCandidates(solutionContext)){
                    // if true is returned, this indicates that a solution was reached - return it
                    return solutionContext.solution;
                }
                postSolvedCount = solutionContext.solvedCellCount;
            }
            while(preSolvedCount !== postSolvedCount);  // Keep going if more cells got solved by candidate elimination.

            /* ********************************************************************** */
            /*                                                                        */           
            /* STEP 3 = If we get here, it means that the puzzle did get solved       */
            /* by our candidate elimination process.  In the early version of the     */
            /* solver, the next step was to look for so-called 'naked twins'.         */
            /* Experimentation found that eliminating naked twins rarely avoided      */
            /* having to rely on brute force to solve the puzzle.  As a result, that  */
            /* complex code was removed from the solver.                              */
            /*                                                                        */  
            /* The only remaining tool at our disposal to reach the solution is to    */
            /* rely on brute-force and try to guess the solution.   In order to       */           
            /* maximize the odds of making the right guess, the algorithm looks for   */
            /* the cell with the fewest candidates & picks a value for it out of its  */           
            /* remaining candidates.  This guessed value is added to the current      */
            /* solution to form a new puzzle that may or may not be valid depending   */
            /* on whether or not the right cell value was guessed.  This new          */
            /* puzzle consisting of [current_solution + guessed cell] is recursively  */
            /* submitted to _doSolve().  If _doSolve() can't reach a solution the     */
            /* guessed cell value will be set to the cell's next available candidate  */
            /* until either a solution is reached or all the guesses for that cell    */
            /* have been exhausted indicating that a solution does not exist.         */
            /*                                                                        */           
            /* NOTE on performance:  every new call of _doSolve() recalculates all    */           
            /* candidates for all the cells.  It would be possible seed these values  */
            /* with the already-computed ones to avoid having to redo these           */
            /* recalculations but because the performance of the solver is already    */
            /* adequate this optimization was not implemented in this iteration.      */
            /* If this were an agile project, we might consider adding in that        */
            /* if performance is proven to be an issue.                               */
            /*                                                                        */           
            /* ********************************************************************** */
            var mostConstrainedCellIndex = this._getCellIndexWithFewestCandidates(solutionContext.cellCandidates);
            for(var j = 0; j < this.constants.NUM_CELL_VALUES; j++){
                if(solutionContext.cellCandidates[mostConstrainedCellIndex] & 1<<j){
                    // 'j' is a possible value of the cell with fewest candidates
                    var modifiedPuzzleInput = solutionContext.solution.slice();     // Make a copy of the current solution
                    modifiedPuzzleInput[mostConstrainedCellIndex] = j + 1;          // Add guess to the solution to form a new puzzle
                    console.log("Guessing that cell index " + mostConstrainedCellIndex + " has value " + modifiedPuzzleInput[mostConstrainedCellIndex]);
                    var solution = this._doSolve(modifiedPuzzleInput);              // Ask _do_solve() to chew in that new puzzle         
                    if(solution !== null){
                        // A solution was reached, return it.
                        return solution;
                    }
                }
            }
            // All the candidates have been tried on the cell with fewest candidates - no solution was found, return null.
            return null;
        }
        catch(e){
            if(e instanceof InvalidSolutiontException){
                console.error("Invalid solution detected: " + e.message);
            }
            console.error("_doSolve error: " + e.message);
            return null;
        }
    };
    
    /**
     * Creates the data structure that is sued to hold the data for a solve.  It contains 
     * bitfield arrays for row, column, cluster and cell candidates as well as the computed
     * solution.
     */
    theSolver.prototype._createNewSolutionContext = function () {
        console.log("_createNewSolutionContext");
        var contextToReturn = {};
        contextToReturn.solvedCellCount  = 0;
        contextToReturn.rowCandidates = [];
        contextToReturn.columnCandidates = [];
        contextToReturn.clusterCandidates = [];
        contextToReturn.cellCandidates = [];
        contextToReturn.solution = [];

        for(var i = 0; i < this.constants.NUM_CELL_ROWS; i++){
            contextToReturn.rowCandidates.push(this.constants.ALL_CANDIDATES_BITFIELD);
            contextToReturn.columnCandidates.push(this.constants.ALL_CANDIDATES_BITFIELD);
            contextToReturn.clusterCandidates.push(this.constants.ALL_CANDIDATES_BITFIELD);
        }
        for(var i = 0; i < this.constants.TOTAL_NUM_CELLS; i++){
            contextToReturn.cellCandidates.push(this.constants.ALL_CANDIDATES_BITFIELD);
            contextToReturn.solution.push(0);
        }
        return contextToReturn;
    };   
    
    /**
     * Adds a cell value to the solution and takes out the value as possible candidate for the
     * cell's row, column and cluster.
     * 
     * @param {Number} solvedCellIndex - 0-based index of cell whose value has been solved
     * @param {Number} solvedCellValue - value of the cell.
     * @param {Object} solutionContext 
     * @returns {Boolean} True if the 'add' opration results in the puzzle being solved
     */
    theSolver.prototype._addToSolution = function(solvedCellIndex, solvedCellValue, solutionContext){
        if(this._isCellSolved(solvedCellIndex, solutionContext)){
            console.info("Solution already reached for cell index " + solvedCellIndex);
            // The cell is already solved - check if the new solution is consistent with the existing one.
            if(solvedCellValue !== solutionContext.solution[solvedCellIndex]){
                // New solution contradicts the existing one - throw exception to inform _doSolve().
                throw new InvalidSolutiontException("AddSolution found cell already solved with a different value.  Cell Index=" + solvedCellIndex + "; new value=" + solvedCellValue + "; old value=" + solutionContext.solution[solvedCellIndex]);
            }
            else{
                // new solution matches existing one, let it slide and just return.
                return;
            }
        }

        // Artificially slow down algorithm which can be sometimes useful when debugging.
        if(typeof SLOW_DOWN_FOR_DEBUGGING !== 'undefined' && SLOW_DOWN_FOR_DEBUGGING){
            var date = new Date();
            var curDate = null;
            do { curDate = new Date(); }
            while(curDate-date < 50);        

        }
        
        // malformed sudoku puzzles or invalid solutions may cause the 
        // number to violate the sudoku rules - check before adding to 
        // solution.  If violation is detected, throw an exception
        if(!this._doesCellAllowValue(solvedCellIndex, solvedCellValue, solutionContext)){
            throw new InvalidSolutiontException("Solved value for cell not found in candidates -> invalid configuration: Cell Index=" + solvedCellIndex + "; value=" + solvedCellValue);
        }
        
        // Cell solution passes all the Sudoku rules, at it to the solution
        solutionContext.solution[solvedCellIndex] = solvedCellValue;
        solutionContext.cellCandidates[solvedCellIndex] = 0;
        solutionContext.solvedCellCount++; 
        
        console.debug("Cell index " + solvedCellIndex + " is " + solvedCellValue + "; solutionCount: " + solutionContext.solvedCellCount);
        // generate progress notification if high water mark is reached and we have a registered progress callback.
        if(this.progressCallback && solutionContext.solvedCellCount > this.solvedCellHighWaterMark){
            this.solvedCellHighWaterMark = solutionContext.solvedCellCount;
            this.progressCallback(solutionContext.solvedCellCount/this.constants.TOTAL_NUM_CELLS);
        }        
        
        // check if we have reached a solution
        if(solutionContext.solvedCellCount === this.constants.TOTAL_NUM_CELLS){
            if(this.progressCallback){
                this.progressCallback(1);
            }        
            var now = new Date().getTime();
            console.debug("Sudoku solved in " + (now - this.startTimestamp) + " ms!");
            return true;
        }

        // Assigning a value to a cell eliminates that value as a candidate in the cell's row, column and cluster.
        // Remove that value from this cell groups.
        var cellRowIndex = this._getRowIndexForCell(solvedCellIndex);
        solutionContext.rowCandidates[cellRowIndex] = this._removeCandidate(solutionContext.rowCandidates[cellRowIndex], solvedCellValue);
        
        var cellColumnIndex = this._getColumnIndexForCell(solvedCellIndex);
        solutionContext.columnCandidates[cellColumnIndex] = this._removeCandidate(solutionContext.columnCandidates[cellColumnIndex], solvedCellValue);
        
        var cellClusterIndex = this._getClusterIndexForCell(solvedCellIndex);
        solutionContext.clusterCandidates[cellClusterIndex] = this._removeCandidate(solutionContext.clusterCandidates[cellClusterIndex], solvedCellValue);
  
  
        // We have now added the solved cell to the solution and recalculated the candidates for the cell's row, column and cluster.
        // Now, look at the unsolved cells in these row, column and cluster and recalculate their candidates.  If any cell no longer
        // has any valid candidates left, it means that we have reached an invalid puzzle state.
        var affectedCellIndicesArray = this._getCellIndicesForRow(cellRowIndex).concat(this._getCellIndicesForColumn(cellColumnIndex));
        affectedCellIndicesArray = affectedCellIndicesArray.concat(this._getCellIndicesForCluster(cellClusterIndex));
        
        for(var i = 0; i < affectedCellIndicesArray.length; i++){
            var tmpCellIndex = affectedCellIndicesArray[i];
            if(!this._isCellSolved(tmpCellIndex, solutionContext) && tmpCellIndex !== solvedCellIndex){
                var candidates = this._calculateCandidatesForCell(tmpCellIndex, solutionContext);
                if(candidates === 0){
                    // There are no valid candidates left for this cell which means that the puzzle cannot be solved with the current
                    // solution, throw exception
                    console.error("Candidate is 0 for cell index " + tmpCellIndex);
                    throw new InvalidSolutiontException("Candidate is 0 for cell index " + tmpCellIndex);
                }
                solutionContext.cellCandidates[tmpCellIndex] = candidates;
            }
        }
    };
    
    /**
     * Go through all the rows, columns and clusters and look for cells with a unique
     * candidate within them.  If a cell is the only one to have value 'x' as a candidate
     * in the whole row, column or cluster then this is the cell's value.
     * 
     * @param {type} solutionContext
     * @returns {Boolean} true is solution is reached; false otherwise
     */
    theSolver.prototype._solveUniqueCandidates = function(solutionContext){
        // start with the rows...
        for(var index = 0; index < this.constants.NUM_CELL_ROWS; index++){
            console.log("Evaluating row " + index);
            var cellIndexArray = this._getCellIndicesForRow(index);
            if(this._solveUniqueCandidatesInCellGroup(cellIndexArray, solutionContext)){
                return true;
            }
        }
            
        // ...then consider the columns...
        for(index = 0; index < this.constants.NUM_CELL_COLUMNS; index++){
            console.log("Evaluating column " + index);
            var cellIndexArray = this._getCellIndicesForColumn(index);
            if(this._solveUniqueCandidatesInCellGroup(cellIndexArray, solutionContext)){
                return true;
            }
        }

        // ...finally, consider the clusters
        for(var index = 0; index < this.constants.NUM_CLUSTERS; index++){
            console.log("Evaluating cluster " + index);
            var cellIndexArray = this._getCellIndicesForCluster(index);
            if(this._solveUniqueCandidatesInCellGroup(cellIndexArray, solutionContext)){
                return true;
            }
        }    
    };

    /**
     * Finds cells within cellIndexArray that are the only ones to possess a
     * given candidate and apply them as the solution when found.  To use a concrete
     * example, if a cell is the only one in its row that can accept value 9 then
     * this cell's value must be 9.
     * 
     * @param {Array} cellIndexArray:  array of cell indices to consider
     * @returns {Boolean}  True if puzzle is solved
     */
    theSolver.prototype._solveUniqueCandidatesInCellGroup = function(cellIndexArray, solutionContext){
        for(var candidateBitPos = 0; candidateBitPos < this.constants.NUM_CELL_VALUES; candidateBitPos++){  // loop though all bit positions of the bitfield
            var uniqueCellIndex = -1;
            for(var i = 0; i < cellIndexArray.length; i++){
                if(solutionContext.cellCandidates[cellIndexArray[i]] & (1 << candidateBitPos)){
                    if(uniqueCellIndex === -1){
                        // first cell with 'candidateBitPos' candidate set, save it in uniqueCellIndex.
                        uniqueCellIndex = cellIndexArray[i];
                    }
                    else{
                        // we found another cell 'candidateBitPos' candidate.  Not unique - this is not what we are lookign for, break for the loop
                        uniqueCellIndex = -1;
                        break;
                    }
                }
            }
            if(uniqueCellIndex !== -1){
                // we found a single cell that has the candidate, it must therefore be its value - add it to the solution.
                if(this._addToSolution(uniqueCellIndex, candidateBitPos + 1, solutionContext)){
                    // this solved the puzzle, return;
                    return true;
                }
            }
        }
        return false;
    };
    
    /*
     * Tests whether a cell value is part of a cell's candidates.
     */
    theSolver.prototype._doesCellAllowValue = function(cellIndex, value, solutionContext){
        return solutionContext.cellCandidates[cellIndex] & 1 << (value - 1);
    }

    /**
     *  tests whether or not a bitfield has a single bit set 
     */
    theSolver.prototype._hasSingleCandidate = function(bitfield){
        return bitfield && !(bitfield & (bitfield - 1));       // (val & (val-1)) will always be 0 for values that are a power of 2 (i.e. single bit set)
    };
    
    /**
     *  returns lowest cell value in candidates.
     */
    theSolver.prototype._getFirstCandidateValueFromBitfield = function(bitfield){
        for(var i = 0; i < this.constants.NUM_CELL_ROWS; i++){
            if(bitfield & (1<<i)){
                return i + 1;
            }
        }
        return 0;        
    };    

    /**
     *   Counts the number of bits set in the bitfield.  
     *   TODO: consider caching candidate counts in a table instead of recaculating if performance is an issue.
     */
    theSolver.prototype._getCandidatesCount = function(bitfield){
        var count = 0;
        for(var i = 0; i < this.constants.NUM_CELL_ROWS; i++){
            if(bitfield & (1<<i)){
                count++;
            }
        }
        return count;        
    }
    
    /**
     *  Goes through an array of cell candidate bitfields and returns the index of the cell
     *  that has the fewest candidates.
     *  
     * @returns {Boolean}  index of cell with fewest candidates
     */
    theSolver.prototype._getCellIndexWithFewestCandidates = function(cellCandidatesArray){
        var smallestCount = this.constants.NUM_CELL_VALUES + 1;
        var cellIndexWithFewestCandidates = 0;
        for(var i = 0; i < cellCandidatesArray.length; i++){
            if(cellCandidatesArray[i] !== 0){       // candidate values of 0 means that cell is solved - skip solved cells
                var count = this._getCandidatesCount(cellCandidatesArray[i]);
                if(count < smallestCount){
                    smallestCount = count;
                    cellIndexWithFewestCandidates = i;
                }
            }
        }
        return cellIndexWithFewestCandidates;
    }

    /**
     *  Removes a cell value from a candidate bitfield
     * 
     * @param {type} bitfield:  value where each bit represents a candidate
     * @param {type} candidate: cell value to exclude from candidates, e.g. [1-9]
     * @returns {Number} bitfield with candidate removed
     */
    theSolver.prototype._removeCandidate = function(bitfield, candidate){
        return bitfield &= ~(1 << (candidate - 1));
    };

    /**
     * Calculates the possible value candidates for the specified cell taking into
     * consideration the available candidates in its row, column and cluster.
     * 
     * @param {type} cellIndex
     * @param {type} solutionContext
     * @returns {Number}  candidates bitfield
     */
    theSolver.prototype._calculateCandidatesForCell = function(cellIndex, solutionContext){
        var cellRowIndex = this._getRowIndexForCell(cellIndex);
        var cellColumnIndex = this._getColumnIndexForCell(cellIndex);
        var cellClusterIndex = this._getClusterIndexForCell(cellIndex);
        return this._calculateCandidatesForBitfields(solutionContext.rowCandidates[cellRowIndex],
                                                     solutionContext.columnCandidates[cellColumnIndex],
                                                     solutionContext.clusterCandidates[cellClusterIndex]);
    };

    /**
     * Finds candidates given a set of candidate bitfields.  This is computed by performing
     * bitwise AND operations on all the supplied bitfields.
     * 
     * @returns candidates bitfield
     */
    theSolver.prototype._calculateCandidatesForBitfields = function(/* two or more bitfields*/){
        var candidates = this.constants.ALL_CANDIDATES_BITFIELD;
        for (var i = 0; i < arguments.length; i++) {
          candidates &= arguments[i];
        }        
        return candidates;
    };
    
    /**
     * This function generates 6 lookup tables that contain computed data that 
     * is used by the solver.  These lookup tables cache frequently-used, calculated 
     * values so that they can be quickly accessed during the solve rather than being 
     * re-computed each time.  The lookup tables map each cell index to their row, column and cluster
     * and they map a row, column and cluster to their constituting cells.  These lookup tables
     * have a negligeable impact on memory.  For a 9x9 Sudoku puzzle, the lookup tables will
     * collectively store 486 64-bit Numbers and allocate 3 Array objects. 
     * 
     * Initialized look-up tables are:
     *      this.cellToClusterLookupTable          // Maps a cell index to the cluster index it is located in
     *      this.cellToRowLookupTable,             // Maps a cell index to the row index it is located in
     *      this.cellToColumnLookupTable,          // Maps a cell index to the column index it is located in
     *      this.cellsInRowLookupTable,            // Maps a row index to an array of the cell indices it contains
     *      this.cellsInColumnLookupTable,         // Maps a column index to an array of the cell indices it contains
     *      this.cellsInClusterLookupTable         // Maps a cluster index to an array of the cluster indices it contains
     */
    theSolver.prototype._initializeCellIndexLookupTables = function(){
        this.cellToClusterLookupTable = [];         // Maps a cell index to the cluster index it is located in
        this.cellToRowLookupTable = [];             // Maps a cell index to the row index it is located in
        this.cellToColumnLookupTable = [];          // Maps a cell index to the column index it is located in
        this.cellsInRowLookupTable = [];            // Maps a row index to an array of the cell indices it contains
        this.cellsInColumnLookupTable = [];         // Maps a column index to an array of the cell indices it contains
        this.cellsInClusterLookupTable = [];        // Maps a cluster index to an array of the cluster indices it contains
        
        for (var i = 0; i < this.constants.TOTAL_NUM_CELLS; i++) {
            // calculate cluster index
            var clusterRow = Math.floor(i / this.constants.NUM_CELLS_IN_CLUSTER_ROW);
            var clusterColumn = Math.floor((i % this.constants.NUM_CELL_ROWS) / this.constants.NUM_CLUSTERS_PER_ROW);
            this.cellToClusterLookupTable.push(clusterRow * this.constants.NUM_CLUSTERS_PER_ROW + clusterColumn);
            
            // calculate row number
            var rowIndex = Math.floor(i / this.constants.NUM_CELL_COLUMNS);
            this.cellToRowLookupTable.push(rowIndex);
            
            // calculate column number
            var columnIndex = i % this.constants.NUM_CELL_COLUMNS;
            this.cellToColumnLookupTable.push(columnIndex);
        }  

        var cellIndexArray;
        for(var rowNumber = 0; rowNumber < this.constants.NUM_CELL_ROWS; rowNumber++){
            var cellIndexArray = new Array();
            cellIndexArray.length = 0;
            var baseOffset = rowNumber * this.constants.NUM_CELL_COLUMNS;
            for(var i = 0; i < this.constants.NUM_CELL_COLUMNS; i++){
                cellIndexArray.push(baseOffset + i);
            }
            this.cellsInRowLookupTable.push(cellIndexArray);
        }
        
        for(var colNumber = 0; colNumber < this.constants.NUM_CELL_COLUMNS; colNumber++){
            var cellIndexArray = new Array();
            for(var i = 0; i < this.constants.NUM_CELL_ROWS; i++){
                cellIndexArray.push(colNumber + (i * this.constants.NUM_CELL_COLUMNS));
            }
            this.cellsInColumnLookupTable.push(cellIndexArray);
        }    
        
        for(var clusterNumber = 0; clusterNumber < this.constants.NUM_CLUSTERS; clusterNumber++){
            var cellIndexArray = new Array();
            baseOffset = this.constants.NUM_CELLS_IN_CLUSTER_ROW * Math.floor(clusterNumber / this.constants.NUM_CLUSTERS_PER_COLUMN) + this.constants.NUM_CELL_COLUMNS_IN_CLUSTER * (clusterNumber % this.constants.NUM_CLUSTERS_PER_ROW);
            for(var i = 0; i < this.constants.NUM_CELLS_IN_CLUSTER; i++){
                cellIndexArray.push(baseOffset + i%this.constants.NUM_CELL_COLUMNS_IN_CLUSTER + this.constants.NUM_CELL_COLUMNS * Math.floor(i / this.constants.NUM_CELL_COLUMNS_IN_CLUSTER));
            }
            this.cellsInClusterLookupTable.push(cellIndexArray);
        }    
    };
    
    /**
     * Returns the index of the cluster in which the supplied cell index is located
     * 
     * @param {Number} cellIndex: 0-based index of cell
     * @returns {Number} 0-based index of encompassing cluster 
     */
    theSolver.prototype._getClusterIndexForCell = function(cellIndex){
        return this.cellToClusterLookupTable[cellIndex];
    };
    
    /**
     * Returns the index of the row in which the supplied cell index is located
     * 
     * @param {Number} cellIndex: 0-based index of cell
     * @returns {Number} 0-based index of encompassing row 
     */
    theSolver.prototype._getRowIndexForCell = function(cellIndex){
        return this.cellToRowLookupTable[cellIndex];
    };

    /**
     * Returns the index of column row in which the supplied cell index is located
     * 
     * @param {Number} cellIndex: 0-based index of cell
     * @returns {Number} 0-based index of encompassing column 
     */
    theSolver.prototype._getColumnIndexForCell = function(cellIndex){
        return this.cellToColumnLookupTable[cellIndex];
    };
      
    /**
     * Returns an array of cell indices that are found within the row designated by its index
     * 
     * @param {Number} rowIndex: 0-based index of row
     * @returns {Array} Array of 0-based cell indices found in the row
     */
    theSolver.prototype._getCellIndicesForRow = function(rowIndex){
        return this.cellsInRowLookupTable[rowIndex];
    };
      
    /**
     * Returns an array of cell indices that are found within the column designated by its index
     * 
     * @param {Number} columnIndex: 0-based index of column
     * @returns {Array} Array of 0-based cell indices found in the column
     */
    theSolver.prototype._getCellIndicesForColumn = function(columnIndex){
        return this.cellsInColumnLookupTable[columnIndex];
    };
      
    /**
     * Returns an array of cell indices that are found within the cluster designated by its index
     * 
     * @param {Number} clusterIndex: 0-based index of cluster
     * @returns {Array} Array of 0-based cell indices found in the cluster
     */
    theSolver.prototype._getCellIndicesForCluster = function(clusterIndex){
        return this.cellsInClusterLookupTable[clusterIndex];
    };
    
    theSolver.prototype._isCellSolved = function(cellIndex, solutionContext){
        if(solutionContext.solution[cellIndex]){
            return true;
        }
        return false;
    };
    
    /**
     * Verifies that the supplied solution complies with the Sudoku rules, i.e., each
     * row, column and cluster all contain unique values ranging from 1 to number of [row, column, cluster].
     * In addition, if a puzzle is supplied, the verifier will also verify that the solution is for that
     * puzzle.
     * 
     * @param {Array} solution: array of numbers representing the solution
     * @param {Array} [optional] puzzle: array of numbers  representing the solution
     * @returns true: valid, false: invalid
     */
    theSolver.prototype._verifySolutionCompliance = function(solution, puzzle){
        for(var cellGroupIndex = 0; cellGroupIndex < this.constants.NUM_CELL_ROWS ; cellGroupIndex++){  //NUM_CELL_ROWS is used to iterate through all rows, colums and clusters.  THis is ok because puzzle is square
            var rowValueBitfield = 0;
            var columnValueBitfield = 0;
            var clusterValueBitfield = 0;
            
            var rowCellIndices = this._getCellIndicesForRow(cellGroupIndex);
            var columnCellIndices = this._getCellIndicesForColumn(cellGroupIndex);
            var clusterCellIndices = this._getCellIndicesForCluster(cellGroupIndex);
            for(var subIndex = 0; subIndex < this.constants.NUM_CELL_VALUES; subIndex++){  // increment count 
                var rowCellValue = solution[rowCellIndices[subIndex]];
                var columnCellValue = solution[columnCellIndices[subIndex]];
                var clusterCellValue = solution[clusterCellIndices[subIndex]];

                // For each value in a row, column or cluster, set the corresponding bit in the bitfield.
                // If the row, column or cluser contains one of each value, the bitfield will amount to
                // this.constants.ALL_CANDIDATES_BITFIELD .
                rowValueBitfield     |= 1 << (rowCellValue - 1);
                columnValueBitfield  |= 1 << (columnCellValue - 1);
                clusterValueBitfield |= 1 << (clusterCellValue - 1);
            }
            
            if(rowValueBitfield     !== this.constants.ALL_CANDIDATES_BITFIELD ||
               columnValueBitfield  !== this.constants.ALL_CANDIDATES_BITFIELD ||
               clusterValueBitfield !== this.constants.ALL_CANDIDATES_BITFIELD){
               console.error("Error in row, col or cluster index " + cellGroupIndex + "; row bitfield: " + rowValueBitfield + "; col bitfield: " + columnValueBitfield + "; cluster bitfield: " + clusterValueBitfield);
               return false;
            }
        }
        
        // If we get here, it means that all the rows, columns and clusters have each
        // possible values and the solution abides by the Sudoku rules.  If a puzzle
        // is supplied, check tha tthe solution is for that puzzle by checking that 
        // each set cell value in the puzzle makes the one in the solution.
        
        if(puzzle){
            for(var index = 0; index < this.constants.TOTAL_NUM_CELLS; index++){
                if(puzzle[index] !== 0){
                    if(puzzle[index] !== solution[index]){
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    /**
     * 
     * @Number: number of cells in puzzle
     * 
     * returns constants object or null if puzzle is not square.
     * 
     */
    theSolver.prototype._initializeConstants = function(numberOfCells){
        // the solver can only work on square grids with the same number of clusters
        // as there are cells within it.  In other words, the fourth root of 'numberOfCells'
        // has to be an integer.
        this.constants = {};
        this.constants.NUM_CELL_ROWS = Math.sqrt(numberOfCells);
        this.constants.NUM_CLUSTERS_PER_ROW = Math.sqrt(this.constants.NUM_CELL_ROWS);
        if(this.constants.NUM_CELL_ROWS === Math.floor(this.constants.NUM_CELL_ROWS) && this.constants.NUM_CLUSTERS_PER_ROW === Math.floor(this.constants.NUM_CLUSTERS_PER_ROW)){
            this.constants.NUM_CELL_VALUES = this.constants.NUM_CELL_ROWS; 
            if(this.constants.NUM_CELL_VALUES > MAX_NUMBER_OF_CELL_VALUES){
                console.log("Requested puzzle is too large");
                return null;
            }
            this.constants.NUM_CELL_ROWS_IN_CLUSTER = this.constants.NUM_CLUSTERS_PER_ROW;
            this.constants.NUM_CLUSTERS_PER_COLUMN = this.constants.NUM_CELL_ROWS_IN_CLUSTER;        
            this.constants.NUM_CELL_COLUMNS_IN_CLUSTER = this.constants.NUM_CELL_ROWS_IN_CLUSTER;
            this.constants.NUM_CELL_COLUMNS = this.constants.NUM_CLUSTERS_PER_ROW * this.constants.NUM_CELL_ROWS_IN_CLUSTER;
            this.constants.NUM_CLUSTERS = this.constants.NUM_CLUSTERS_PER_ROW  * this.constants.NUM_CLUSTERS_PER_ROW ;
            this.constants.NUM_CELLS_IN_CLUSTER = this.constants.NUM_CELL_ROWS_IN_CLUSTER * this.constants.NUM_CELL_COLUMNS_IN_CLUSTER;
            this.constants.NUM_CELLS_IN_CLUSTER_ROW = this.constants.NUM_CELLS_IN_CLUSTER * this.constants.NUM_CLUSTERS_PER_ROW;
            this.constants.TOTAL_NUM_CELLS = this.constants.NUM_CELL_ROWS * this.constants.NUM_CELL_COLUMNS;
            // Compute the bit field that has one bit set for every possible cell value.
            // In a 9x9 Sudoku puzlle, this amounts to binary value 111111111. This 
            // value is used throughout the algorithm so it's a good idea to pre-compute it.
            this.constants.ALL_CANDIDATES_BITFIELD = (1 << this.constants.NUM_CELL_VALUES) - 1;

            return this.constants;
        }
        else{
            console.error('Irregular puzzle shape - return null to signify error');
            return null;
        }   
    }
      
    return theSolver; 
}());
