importScripts('solver.js');

/**
 * The SolverThread is a simple component that exchanges messages with its creator
 * to initiate solves and report on progress and solution. The actual solving 
 * work is performed by the Solver().
 */


self.addEventListener('message', handleMessage, false);
self.solutionId;

function handleMessage(event) {
    var data = event.data;

    console.error("SolverThread: command: " + data.cmd);
    switch (data.cmd) {
        case 'setSolutionId':
            self.solutionId = data.id;
            break;
        case 'solve':
            var solver = new Solver();
            var solution = solver.solve(data.puzzle, onProgressReport);
            self.postMessage({'cmd': 'solveResult', 'id': self.solutionId, 'solution': solution});
            self.close(); // Puzzle is solved - no need to the thread anymore
            break;
        default:
            console.error("SolverThread: unknown command: " + data.cmd);
    }
    ;
}

function onProgressReport(percentage) {
    self.postMessage({'cmd': 'progress', 'id': self.solutionId, 'percent': percentage});
}


