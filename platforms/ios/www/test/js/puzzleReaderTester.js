QUnit.test( "PuzzleReader - undefined URL", function( assert ) {
    var url; /* undefined */
    assert.throws( function(){
        sudoqu.PuzzleReader.getInstance().readFromUrl(url,  function(puzzleArray){
            assert.ok(0, "Callback should not be called");
        });
    }, "undefined URL throws");
    
});

QUnit.test( "PuzzleReader - undefined URL callback", function( assert ) {
    var url = 'res/puzzles/RuntPuzzle.txt';
    assert.throws( function(){
        sudoqu.PuzzleReader.getInstance().readFromUrl(url, null);
    }, "undefined URL callback throws");
});

QUnit.test( "PuzzleReader - undefined string", function( assert ) {
    var string; /* undefined */
    assert.throws( function(){
        sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
            assert.ok(0, "Callback should not be called");
        });
    }, "undefined string throws");
});

QUnit.test( "PuzzleReader - undefined string callback", function( assert ) {
    var string = '123456789.........123456789.........123456789.........123456789.........123456789';
    assert.throws( function(){
        sudoqu.PuzzleReader.getInstance().readFromUrl(string, null);
    }, "undefined string callback throws");
});

QUnit.test( "PuzzleReader - handle runt puzzles from URL", function( assert ) {
    var done = assert.async();
    var url = 'res/puzzles/RuntPuzzle.txt';
    sudoqu.PuzzleReader.getInstance().readFromUrl(url,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "runt puzzle from URL detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle runt puzzles from string", function( assert ) {
    var done = assert.async();
    var string = '123456789.........123456789.........123456789.........123456789.........12345678';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "runt puzzle from string detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle oversized from URL", function( assert ) {
    var done = assert.async();
    var url = 'res/puzzles/OversizedPuzzle.txt';
    sudoqu.PuzzleReader.getInstance().readFromUrl(url,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "oversized from URL detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle oversized puzzles from string", function( assert ) {
    var done = assert.async();
    var string = '123456789.........123456789.........123456789.........123456789.........1234567899';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "oversized puzzle from string detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle puzzle with invalid character from URL", function( assert ) {
    var done = assert.async();
    var url = 'res/puzzles/PuzzleWithInvalidCharacters.txt';
    sudoqu.PuzzleReader.getInstance().readFromUrl(url,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "puzzle with invalid character from URL detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle puzzle with invalid character from string", function( assert ) {
    var done = assert.async();
    var string = '12345*789.........123456789.........123456789.........123456789.........123456789';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 0, "puzzle with invalid character from string detected");
        done();
    });
});

QUnit.test( "PuzzleReader - handle valid puzzle from String", function( assert ) {
    var done = assert.async();
    var string = '111111111111111111111111111111111111111111111111111111111111111111111111111111111';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 1, "puzzle read correctly");
        assert.equal(puzzleArray[0].length, 81, "puzzle has correct length");
        for(var i = 0; i < puzzleArray[0].length; i++){
            assert.equal(puzzleArray[0][i], 1);
        }
        done();
    });
});

QUnit.test( "PuzzleReader - read from String 2 valid puzzles separated by LF", function( assert ) {
    var done = assert.async();
    var string = '111111111111111111111111111111111111111111111111111111111111111111111111111111111\n222222222222222222222222222222222222222222222222222222222222222222222222222222222';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 2, "puzzles read correctly");
        assert.equal(puzzleArray[0].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[1].length, 81, "puzzle has correct length");
        for(var i = 0; i < puzzleArray[0].length; i++){
            assert.equal(puzzleArray[0][i], 1);
            assert.equal(puzzleArray[1][i], 2);
        }
        done();
    });
});

QUnit.test( "PuzzleReader - read from String 2 valid puzzles separated by CRLF", function( assert ) {
    var done = assert.async();
    var string = '111111111111111111111111111111111111111111111111111111111111111111111111111111111\r\n222222222222222222222222222222222222222222222222222222222222222222222222222222222';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 2, "puzzles read correctly");
        assert.equal(puzzleArray[0].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[1].length, 81, "puzzle has correct length");
        for(var i = 0; i < puzzleArray[0].length; i++){
            assert.equal(puzzleArray[0][i], 1);
            assert.equal(puzzleArray[1][i], 2);
        }
        done();
    });
});

QUnit.test( "PuzzleReader - verify that leading and trailing white psaces are properly ignores", function( assert ) {
    var done = assert.async();
    var string = '    \t    \t \t  111111111111111111111111111111111111111111111111111111111111111111111111111111111     \t\t\r\n\t   222222222222222222222222222222222222222222222222222222222222222222222222222222222\t\t\t ';
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 2, "puzzles read correctly");
        assert.equal(puzzleArray[0].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[1].length, 81, "puzzle has correct length");
        for(var i = 0; i < puzzleArray[0].length; i++){
            assert.equal(puzzleArray[0][i], 1);
            assert.equal(puzzleArray[1][i], 2);
        }
        done();
    });
});

QUnit.test( "PuzzleReader - verify that only good puzzles are returned", function( assert ) {
    var done = assert.async();
    var string = " \
*********************************************************************************\n\
*********************************************************************************\n\
111111111111111111111111111111111111111111111111111111111111111111111111111111111\n\
*********************************************************************************\n\
222222222222222222222222222222222222222222222222222222222222222222222222222222222\n\
333333333333333333333333333333333333333333333333333333333333333333333333333333333\n\
*********************************************************************************\n\
444444444444444444444444444444444444444444444444444444444444444444444444444444444\n\
555555555555555555555555555555555555555555555555555555555555555555555555555555555\n\
*********************************************************************************\n\
*********************************************************************************\n\
666666666666666666666666666666666666666666666666666666666666666666666666666666666\n\
*********************************************************************************\n\
777777777777777777777777777777777777777777777777777777777777777777777777777777777\n\
888888888888888888888888888888888888888888888888888888888888888888888888888888888\n\
*********************************************************************************\n\
999999999999999999999999999999999999999999999999999999999999999999999999999999999\n\
*********************************************************************************\n"
        
    sudoqu.PuzzleReader.getInstance().readFromString(string,  function(puzzleArray){
        assert.equal(puzzleArray.length, 9, "puzzles read correctly");
        assert.equal(puzzleArray[0].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[1].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[2].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[3].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[4].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[5].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[6].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[7].length, 81, "puzzle has correct length");
        assert.equal(puzzleArray[8].length, 81, "puzzle has correct length");
        for(var i = 0; i < puzzleArray[0].length; i++){
            assert.equal(puzzleArray[0][i], 1);
            assert.equal(puzzleArray[1][i], 2);
            assert.equal(puzzleArray[2][i], 3);
            assert.equal(puzzleArray[3][i], 4);
            assert.equal(puzzleArray[4][i], 5);
            assert.equal(puzzleArray[5][i], 6);
            assert.equal(puzzleArray[6][i], 7);
            assert.equal(puzzleArray[7][i], 8);
            assert.equal(puzzleArray[8][i], 9);
        }
        done();
    });
});


